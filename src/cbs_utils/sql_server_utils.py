import io
import logging

import pandas as pd
import pyodbc
import yaml
from cbs_utils.misc import CacheInfo

from .readers import (store_dataframe_to_cache, read_dataframe_from_cache)

logger = logging.getLogger(__name__)


def make_compound_sql_command(tables, max_rows=None, join_key=None, filters=None,
                              order_by=None, distinct=True):
    """
    Make a sql command combining the info of multiple columns

    Parameters
    ----------
    tables: dict
        Information of query per table
    max_rows: int
        Limit number of rows
    join_key:

    Returns
    -------

    """
    if max_rows is not None:
        top_sel = f"\nTOP({max_rows})"
    else:
        top_sel = ""

    all_columns = None
    col_col = list()

    for table_key, table_info in tables.items():
        apply_selection = table_info.get('apply_selection', True)
        if not apply_selection:
            columns = None
        else:
            columns = table_info.get('columns')
        # maak de column en rij selecties
        if columns is not None:
            used = set()
            unique = [x for x in columns if x not in used and (used.add(x) or True)]
            col_sel = ",\n".join([f"{table_key}.[{col}]" for col in unique if col not in col_col])
            col_col.extend(unique)
        else:
            col_sel = f"{table_key}.[*]"

        if all_columns is None:
            all_columns = col_sel
        else:
            all_columns += ",\n" + col_sel

    # bouw het sql command
    sql_command = None
    prev_table_key = None
    first_table_key = None
    if isinstance(join_key, str):
        join_keys_global = [join_key]
    else:
        join_keys_global = join_key
    for table_key, table_info in tables.items():
        table_name = table_info["naam"]
        join_keys = table_info.get("join_key", join_keys_global)
        if isinstance(join_keys, str):
            join_keys = [join_keys]

        if first_table_key is None:
            first_table_key = table_key

        if distinct:
            dist = "DISTINCT"
        else:
            dist = ""

        if sql_command is None:
            sql_command = f"SELECT {dist} {top_sel}" \
                          f"\n{all_columns}\nFROM {table_name} {table_key}"
        else:
            if isinstance(table_info, str):
                join_keys = [table_info]
            join = None
            for cnt, jk in enumerate(join_keys):
                this_key = f"{table_key}.[{jk}]"
                prev_key = f"{prev_table_key}.[{jk}]"
                if join is None:
                    join = f"\nINNER JOIN {table_name} {table_key}\nON {prev_key} = {this_key}"
                else:
                    join += f" AND {prev_key} = {this_key}"

            sql_command += join

        prev_table_key = table_key

    if filters is not None:
        sql_command += "\nWHERE"
        for cnt, (name, value) in enumerate(filters.items()):
            if cnt > 0:
                sql_command += "\nAND "
            if "null" in str(value).lower():
                equal = "is"
            else:
                equal = "="
            sql_command += f"\n{first_table_key}.{name} {equal} {value}"
    if order_by:
        if isinstance(order_by, str):
            order = order_by
        else:
            order = ",".join(order_by)
        sql_command += f"\nORDER BY {order}"
    sql_command += ";"

    return sql_command


def make_sql_command(table_name, count_rows=False, columns=None, max_rows=None, filters=None,
                     order_by=None, distinct=True):
    """
    Make het sql commando.

    Parameters
    ----------
    table_name: str
        Naam van de tabel
    count_rows: bools, optional
        Tel alleen de regels
    columns: list, optional
        Selecteer alleen een aantal kolommen. Als None, selecteer alles
    max_rows: int, optional
        Limiteer het maximaal aantal regels
    filters: dict, optional
        Dict met filters

    Returns
    -------
    str:
        String met het sql commando

    """

    # maak de column en rij selecties
    if columns is not None:
        col_sel = ",".join([f"[{col}]" for col in columns])
    else:
        col_sel = "*"

    if count_rows:
        col_sel = f"COUNT({col_sel})"

    if max_rows is not None:
        top_sel = f"TOP({max_rows})"
    else:
        top_sel = ""

    # bouw het sql command
    if distinct:
        dist = "DISTINCT"
    else:
        dist = ""
    sql_command = f"SELECT {dist} {top_sel} {col_sel} FROM {table_name}"

    if filters is not None:
        sql_command += " WHERE"
        for cnt, (name, value) in enumerate(filters.items()):
            if cnt > 0:
                sql_command += " and "
            if "null" in str(value).lower():
                equal = "is"
            else:
                equal = "="
            sql_command += f" {name} {equal} {value}"

    if order_by:
        if isinstance(order_by, str):
            order = order_by
        else:
            order = ",".join(order_by)
        sql_command += f"\nORDER BY {order}"

    sql_command += ";"

    return sql_command


def clean_and_set_index(data, set_index):
    """

    Parameters
    ----------
    data: DataFrame
        Dataframe met de data
    set_index: dict
         instellingen om de index te zetten

    Returns
    -------
    DataFrame:
        Nieuwe dataframe

    """
    key = set_index["key"]
    # verwijder eerst de nans
    is_nan = data[key].isna()
    if is_nan.sum() > 0:
        data = data[~is_nan]

    sort_by = set_index.get("sort_by")
    if sort_by is not None:
        if isinstance(sort_by, dict):
            sort_keys = list(sort_by.keys())
            sort_order = list(sort_by.values())
        else:
            sort_keys = sort_by
            sort_order = None
        data.sort_values(by=sort_keys, ascending=sort_order, inplace=True)
    is_duplicate_key = data[key].duplicated()
    if is_duplicate_key.sum() > 0:
        data = data[~is_duplicate_key]
    return data.set_index(key, drop=True)


class SqlServerReader(object):
    """
    Klasse om een sql server data base te openen en de data van een tabel te laden

    Parameters
    ----------
    driver: str, optional
        De naam van de sql server driver. Default: "ODBC Driver 13 for SQL Server"
    server: str
        De naam van de server die we willen benaderen, zoals "s0dsql0121p\\i01"
    database: str
        Naam van de database die je wilt laden
    table_name: str
        Naam van de tabel
    reset_cache: bool, optional
        Laat de sql data opnieuw en sla op naar cache. Default = False, zodat bij een tweede run
        de data uit de cache gelezen worden
    columns: list, optional
        Lijst van columnen die je wilt laden. Default = None, dan laad je alles
    max_rows: int, optional
       Het maximum aantal rijen dat gelezen wordt. Default = None, dwz laat alles
    filters: dict, optional
        Dictionary met kolom_naam:kolom_waarde paren waarop je wilt filteren bij het laden van de
        sql server. Zo kan je een selectie van rijen maken. Default = None, dus filter niks
    order_by: list, optional
        We kunnen ook sorteren op deze keys
    read_data: bool
        Als deze False is laat je alleen de database informatie zien, maar  laad je de data niet
    """

    def __init__(self,
                 driver: str = None,
                 server: str = None,
                 database: str = None,
                 table_info: dict = None,
                 sql_command: str = None,
                 join_key: str = None,
                 columns: list = None,
                 cache_info: CacheInfo = None,
                 reset_cache: bool = False,
                 max_rows: int = None,
                 filters: dict = None,
                 order_by: list = None,
                 distinct: bool = True,
                 read_data: bool = True,
                 show_meta_data: bool = False,
                 dry_run: bool = False,
                 renames: dict = None,
                 set_index: dict = None
                 ):

        if driver is None:
            driver = "ODBC Driver 13 for SQL Server"

        # bouw de string om connectie te leggen
        self.connection_info = f"DRIVER={driver};" \
                               f"SERVER={server};" \
                               f"DATABASE={database};" \
                               f"Trusted_Connection=yes"
        logger.info("Start connection to the sql server database:\n{}".format(self.connection_info))

        self.dry_run = dry_run
        self.columns = columns
        self.max_rows = max_rows
        self.filters = filters
        self.order_by = order_by
        self.set_index = set_index
        self.distinct = distinct
        self.table_info = table_info
        self.join_key = join_key
        self.sql_command = sql_command
        self.renames = renames

        if isinstance(table_info, str):
            self.table_name = table_info
        else:
            self.table_name = None

        self.reset = reset_cache

        if cache_info is None:
            self.cache_info = None
        else:
            self.cache_info: CacheInfo = cache_info
            # update read from cache
            self.cache_info.set_read_from_cache_flag(reset_cache)

        self.db_columns = list()
        self.data = None
        self.table_description = None
        self.number_of_rows = None

        if isinstance(table_info, str):
            self.get_table_info(self.table_info)
        else:
            for key, prop in table_info.items():
                name = prop["naam"]
                self.get_table_info(table_name=name)
                break

        if show_meta_data and not self.dry_run:
            self.show_meta_data(table_name=self.table_name)

        self.check_column_names()

        if read_data:
            self.read_data()

    def check_column_names(self):
        """
        Als we een selectie van kolommen meegeven hebben, check of al deze kolommen ook in de
        database gevonden werden.
        """

        if self.columns is not None:
            is_subset = set(self.columns).issubset(set(self.db_columns))
            if not is_subset:
                overlap = set(self.columns).intersection(self.db_columns)
                missing = set(self.columns).difference(overlap)
                raise ValueError(f"Not all columns available in database.\n Missing: {missing}")

    def get_table_info(self, table_name):
        """
        Dump de namen van de kollommen en aantal rijen van de tabel
        """
        if self.cache_info is None or not self.cache_info.read_from_cache:
            sqlcmd1 = "SELECT TOP(1) * FROM {table_name}".format(table_name=table_name)
            logger.debug(f"EXECUTE: {sqlcmd1}")
            if not self.dry_run:
                with pyodbc.connect(self.connection_info) as connection:
                    # eerste rij laden. De column namen kunnen we in de cursor description vinden
                    cursor = connection.execute(sqlcmd1)
                    self.table_description = cursor.description

            sqlcmd2 = make_sql_command(table_name=table_name, count_rows=True,
                                       filters=self.filters, distinct=self.distinct)
            if not self.dry_run:
                with pyodbc.connect(self.connection_info) as connection:
                    logger.debug(f"EXECUTE: {sqlcmd2}")
                    cursor = connection.execute(sqlcmd2)
                    self.number_of_rows = cursor.fetchone()[0]
                self.db_columns = [entry[0] for entry in self.table_description]
                meta_data = dict(
                    columns=self.db_columns,
                    number_of_rows=self.number_of_rows,
                    filters=self.filters,
                )
                if self.cache_info is not None:
                    cache_meta_file = self.cache_info.cache_meta_file
                    logger.info(f"Writing meta data {cache_meta_file}")
                    with open(cache_meta_file, "w") as stream:
                        yaml.dump(meta_data, stream=stream, default_flow_style=False)
        else:
            cache_meta_file = self.cache_info.cache_meta_file
            logger.info(f"Reading meta data from {cache_meta_file}")
            if not self.dry_run:
                with open(cache_meta_file) as stream:
                    meta_data = yaml.load(stream=stream, Loader=yaml.Loader)
                    self.db_columns = meta_data["columns"]
                    self.number_of_rows = meta_data["number_of_rows"]
                    filters = meta_data["filters"]
                    assert filters == self.filters

    def show_meta_data(self, table_name):
        """ Print information about the table to screen """
        logger.info(f" INFO {table_name} ".center(80, "*"))
        logger.info(f"Number of rows in table: {self.number_of_rows}")
        logger.info(f"Columns:")
        for cnt, name in enumerate(self.db_columns):
            logger.info("{:4d} : {}".format(cnt, name))
        logger.info(f" END ".center(80, "*"))

    def read_data(self):
        """
        Lees de data provider data. Als er een cache file bestaat en reset = False, dan lezen we
        direct uit de cache
        """
        if self.cache_info is None or not self.cache_info.read_from_cache:

            if self.sql_command is None:
                if self.table_name is not None:
                    sql_command = make_sql_command(table_name=self.table_name, columns=self.columns,
                                                   filters=self.filters, max_rows=self.max_rows,
                                                   order_by=self.order_by, distinct=self.distinct)
                else:
                    sql_command = make_compound_sql_command(tables=self.table_info,
                                                            join_key=self.join_key,
                                                            filters=self.filters,
                                                            max_rows=self.max_rows,
                                                            order_by=self.order_by,
                                                            distinct=self.distinct)
            else:
                sql_command = self.sql_command

            logger.info("Reading from sql database. Execute:\n")
            logger.info("BEGIN SQL".center(80, "+"))
            logger.info(f"\n\n{sql_command}\n")
            logger.info(" END SQL ".center(80, "-"))
            if self.cache_info is not None:
                sql_out = self.cache_info.file_name.with_suffix(".sql")
                logger.info(f"Writing sql command to {sql_out}")
                with open(str(sql_out), mode="w") as stream:
                    stream.write(sql_command)

            # lees de data van de data base
            if not self.dry_run:
                with pyodbc.connect(self.connection_info) as connection:
                    self.data = pd.read_sql(sql_command, con=connection, columns=self.columns)

                if self.renames is not None:
                    rename = {v: k for k, v in self.renames.items()}
                    self.data.rename(columns=rename, inplace=True)

                if self.set_index is not None:
                    self.data = clean_and_set_index(self.data, self.set_index)

                if self.cache_info is not None:
                    # schrijf de data direct naar een cache file
                    cache_file = self.cache_info.cache_file
                    logger.info(f"Writing to {cache_file}")
                    store_dataframe_to_cache(self.data, str(cache_file), compression=self.cache_info.compression,
                                             cache_type=self.cache_info.file_type)

                    self.cache_info.set_read_from_cache_flag(self.reset)
        else:
            # we hebben al een cache file, dus lees deze
            if self.cache_info.compression:
                cache_file = self.cache_info.compressed_cache_file
            else:
                cache_file = self.cache_info.cache_file
            logger.info("Reading data from cache: {}".format(str(cache_file)))
            if not self.dry_run:
                self.data = read_dataframe_from_cache(file_name=str(cache_file),
                                                      cache_type=self.cache_info.file_type)
        logger.debug(f"Done reading Data provider table for {self.filters}")

    def make_report(self):
        if self.data is not None:
            buf = io.StringIO()
            self.data.info(buf=buf)
            logger.info("INFO:\n{}".format(buf.getvalue()))
            logger.info("HEAD:\n{}".format(self.data.head()))
        else:
            logger.info("No data has been loaded yet")


class PklDataReader(object):
    def __init__(self,
                 file_name: str,
                 reset_cache: bool = False,
                 filters: dict = None,
                 columns: list = None,
                 separator: str = None,
                 max_rows: bool = None,
                 cache_info: CacheInfo = None,
                 show_meta_data: bool = None,
                 renames: dict = None,
                 set_index: dict = None
                 ):
        self.file_name = file_name
        self.reset_cache = reset_cache
        self.filters = filters
        self.columns = columns
        if separator is None:
            self.separator = ","
        else:
            self.separator = separator
        self.max_rows = max_rows
        self.renames = renames
        self.show_meta_data = show_meta_data
        self.set_index = set_index
        if cache_info is None:
            self.cache_info = CacheInfo()
        else:
            self.cache_info: CacheInfo = cache_info
        self.cache_info.set_read_from_cache_flag(reset_cache)

        self.data = None

        self.read_the_data()

        if show_meta_data:
            self.make_report()

    def read_the_data(self):
        if not self.cache_info.read_from_cache:

            if ".pkl" in self.file_name.suffixes:
                self.data = pd.read_pickle(self.file_name)
            elif ".csv" in self.file_name.suffixes:
                try:
                    self.data = pd.read_csv(str(self.file_name), sep=self.separator)
                except UnicodeDecodeError as err:
                    logger.warning(f"Failed to read {self.file_name}: {err}"
                                   f"\nTrye again with codecs")
                    with open(filename=str(self.file_name), encoding='utf-8', errors='ignore') as f:
                        self.data = pd.read_csv(f, sep=self.separator)

            else:
                logger.warning(f"data type not recognised: {self.file_name}. Skipping")

            if self.renames is not None:
                rename = {v: k for k, v in self.renames.items()}
                self.data.rename(columns=rename, inplace=True)

            if self.set_index is not None:
                self.data = clean_and_set_index(self.data, self.set_index)

            # schrijf de data direct naar een cache file
            cache_file = self.cache_info.cache_file
            store_dataframe_to_cache(self.data, str(cache_file),
                                     cache_type=self.cache_info.file_type)
        else:
            logger.info("Reading data from cache: {}".format(str(self.cache_info.cache_file)))
            self.data = read_dataframe_from_cache(file_name=str(self.cache_info.cache_file),
                                                  cache_type=self.cache_info.file_type)

    def make_report(self):
        buf = io.StringIO()
        self.data.info(buf=buf)
        logger.info("INFO:\n{}".format(buf.getvalue()))
        logger.info("HEAD:\n{}".format(self.data.head()))
