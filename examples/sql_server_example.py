"""
Script om de dataprovider/kvk_url_finder data te importeren en te analyseren.

Gebruik:

python sql_server_example.py sql_server_wsettings.yml


De yaml file process_settings.yml bevat alle instellingen. Standaard worden de gegevens van de
geïmporteerde data naar cache pickle file geschreven, zodat je de tweede keer dat je runt niet de
sql server database benadert maar de cache file kan lezen (dat gaat bijna 10 x zo snel).
Als je de cache wilt verversen (nodig als je een andere selectie van data wilt maken dan kan je de
--reset optie meegeven

"""
import argparse
import logging
import os
from pathlib import Path

import pandas as pd
from pandas import DataFrame
import sys
import yaml

from cbs_utils.readers import (store_dataframe_to_cache, read_dataframe_from_cache)
from cbs_utils.sql_server_utils import (SqlServerReader, PklDataReader)
from cbs_utils.misc import (CacheInfo, clean_up_name)

DEFAULT_CACHE_DIRECTORY = "cache"
DEFAULT_CACHE_TYPE = ".pkl"

DEFAULT_DRIVER = "ODBC Driver 13 for SQL Server"
DEFAULT_SERVER = "s0dsql0121p\\i01"
DEFAULT_DATABASE = "EenhScrape_ANA"
DEFAULT_TABLE = "[dbo].[URL.DataProvider]"

logging.basicConfig(level=logging.ERROR,
                    format="%(filename)10s[ln %(lineno)4s] - %(levelname)8s: %(message)s")
logger = logging.getLogger()


def set_log_level(log_level):
    # it is import to keep this function at the top of the module
    logger.setLevel(log_level)


def update_prop_with_template(data_base_prop, template):
    """
    Kopieer recursief de eigenschappen van een dictionary

    Waardes die in de template dict bestaand en in de data_base_prop dictionary niet worden over genomen.
    Als de waarde van het veld ook weer een dictionary is dan vul je deze recursief
    Als een waarde in al in de data_base_prop bestaat maar None is dan wordt deze key gewist
    """
    for key, val in template.items():
        if key not in list(data_base_prop.keys()):
            data_base_prop[key] = val
        else:
            if isinstance(val, dict):
                data_base_prop[key] = update_prop_with_template(data_base_prop[key], val)
            elif data_base_prop[key] is None:
                del data_base_prop[key]

    return data_base_prop


def parse_the_command_line(args):
    """ laad de command line argumenten """
    parser = argparse.ArgumentParser(description="Read url data and process",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("configuration_file", help="Default configuration file")
    parser.add_argument("-d", "--debug", help="Print extra debugging information", dest="log_level",
                        action="store_const", const=logging.DEBUG, default=logging.INFO)
    parser.add_argument("-v", "--verbose", help="Show processing information", dest="log_level",
                        action="store_const", const=logging.INFO, default=logging.INFO)
    parser.add_argument("-q", "--quiet", help="Be quiet during processing", dest="log_level",
                        action="store_const", const=logging.WARNING)
    parser.add_argument("-r", "--reset_cache", action="store_true", help="Reset the cache files ")
    parser.add_argument("--max_rows", help="Limit the number of urls to import to this maximum")
    parser.add_argument("--show_meta_data", help="Show the information of table in the data base",
                        action="store_true")
    parser.add_argument("--read_data", help="Read the data", default=True, action="store_true")
    parser.add_argument("--no_read_data", help="Read the data", dest="read_data",
                        action="store_false")
    parser.add_argument("--levering_id", help="Set the leverings id")
    parser.add_argument("--force", help="Force to process all databases", action='store_true')
    parser.add_argument("--apply_selection", help="Apply the selection on the columns",
                        action='store_true', default=None)
    parser.add_argument("--no_apply_selection", help="Do not apply the selection on the columns",
                        action='store_false', dest="apply_selection")
    parser.add_argument("--dry_run", help="Only show the sql command, do not run yet",
                        action='store_true', default=False)
    parser.add_argument("--cache_type", help="Overrule the cache type", choices={"pkl", "sqlite"})
    parser.add_argument("--update_databases", help="Reread all the databases", action='store_true')
    parser.add_argument("--process_databases", help="Apply all the operation on the databases", action='store_true')
    parser.add_argument("--no_process_databases", help="Do not apply all the operation on the databases",
                        action='store_false', dest='process_databases')
    parser.add_argument("--calculate_statistics", help="Calculate the statistics", action='store_true')
    parser.add_argument("--no_calculate_statistics", help="Do not calculate the statistics",
                        action='store_false', dest='calculate_statistics')

    parsed_arguments = parser.parse_args(args)

    return parsed_arguments, parser


def set_new_index(data, index_name):
    """
    Set the index as index of the data frame
    Parameters
    ----------
    data: DataFrame
        The data frame
    index_name: str
        The name of the index to set

    Returns
    -------
    DataFrame

    """
    if index_name is not None:
        current_index = data.index.name
        if current_index in data.columns:
            logger.debug(f"Index {index_name} already in columns. Drop first")
            data.drop([current_index], inplace=True, axis=1)
        if current_index == index_name:
            logger.info(f"Current index is already set to {index_name}")
        else:
            logger.info(f"New index from {current_index} -> {index_name}")
            data.reset_index(inplace=True)
            data.dropna(axis=0, subset=[index_name], inplace=True)
            data = data.sort_values(index_name)
            duplicated = data[index_name].duplicated()
            data = data[~duplicated]
            data.set_index(index_name, drop=True, inplace=True)
    else:
        current_index = data.index.name
        if current_index is None:
            logger.warning(f"You did not specify a index and the current index is None")
        logger.debug(f"Keeping index name {current_index}")

    return data


def main(args_in):
    args, parser = parse_the_command_line(args_in)

    set_log_level(args.log_level)

    # report start time
    script_name = os.path.basename(sys.argv[0])
    start_time = pd.Timestamp.now(tz="Europe/Amsterdam")
    logger.info(f"Starting {script_name} at {start_time}")
    logger.debug("python version: {version}".format(version=sys.version))
    logger.debug("pandas version: {version}".format(version=pd.__version__))

    with open(args.configuration_file) as stream:
        settings = yaml.load(stream=stream, Loader=yaml.Loader)

    general_settings = settings["general"]
    database_templates = settings["database_templates"]
    databases = settings["databases"]
    output_settings = general_settings["output"]
    cache_settings = general_settings["cache"]
    sql_server_settings = general_settings["sql_server"]
    option_settings = general_settings["options"]

    driver_global = sql_server_settings.get("driver", DEFAULT_DRIVER)
    server_global = sql_server_settings.get("server", DEFAULT_SERVER)
    database_global = sql_server_settings.get("database", DEFAULT_DATABASE)
    table_name_global = sql_server_settings.get("table", DEFAULT_TABLE)

    cache_dir = cache_settings.get("directory", DEFAULT_CACHE_DIRECTORY)
    cache_file = cache_settings.get("file_name")
    cache_type = cache_settings.get("file_type", DEFAULT_CACHE_TYPE)

    if args.cache_type is not None:
        cache_type = args.cache_type

    if cache_file is None:
        # als er geen cache file naam gegeven is dan baseren we het op de database en tabel naam
        cache_file = "_".join([clean_up_name(database_global), clean_up_name(table_name_global)])

    if args.max_rows is not None:
        max_rows = args.max_rows
    else:
        max_rows = option_settings.get("max_rows")

    if args.show_meta_data is not None:
        show_meta_data = args.show_meta_data
    else:
        show_meta_data = option_settings.get("show_meta_data", False)

    if args.read_data is not None:
        read_data = args.read_data
    else:
        read_data = option_settings.get("read_data", True)

    # the micro data cache na het inlezen en combineren van alle databases
    output_directory = Path(output_settings["directory"])
    output_file = Path(output_settings["file_name"])
    file_name = output_directory / output_file

    if args.reset_cache:
        reset_db_cache = True
    else:
        reset_db_cache = False

    for data_base_key, data_base_prop in databases.items():
        if not data_base_prop.get("process_this", True) and not args.force:
            logger.debug(f"Skipping data base {data_base_key}")
            continue
        database_template = data_base_prop.get("template")
        if database_template is not None:
            try:
                template = database_templates[database_template]
            except KeyError:
                raise KeyError(f"Could not find data base template {database_template}")
            data_base_prop = update_prop_with_template(data_base_prop, template)

        logger.info(f"Processing data base {data_base_key}")

        set_index = data_base_prop.get("set_index")

        cache_type = data_base_prop.get('cache_type', cache_type)

        cache_file_name = "_".join([cache_file, data_base_key])

        # CacheInfo is gewoon een klasses om alle cache eigenschappen op te slaan
        cache_info = CacheInfo(file_name=cache_file_name, directory=cache_dir, file_type=cache_type)

        sql_server_prop = data_base_prop["sql_server_database"]

        driver = sql_server_prop.get("driver", driver_global)
        server = sql_server_prop.get("server", server_global)
        database = sql_server_prop.get("database", database_global)
        join_key = sql_server_prop.get("join_key")
        renames = sql_server_prop.get("renames")
        table_info = sql_server_prop.get("table")

        sql_command = sql_server_prop.get("sql_command")

        filters = sql_server_prop.get("filters")
        order_by = sql_server_prop.get("order_by")
        distinct = sql_server_prop.get("distinct")
        apply_selection = sql_server_prop.get("apply_selection", True)
        if apply_selection:
            columns = sql_server_prop.get("columns")
            logger.debug("Apply selection with columns:\n{}".format(columns))
        else:
            columns = None

        sql_server = SqlServerReader(
            driver=driver,
            server=server,
            database=database,
            table_info=table_info,
            sql_command=sql_command,
            join_key=join_key,
            read_data=read_data,
            reset_cache=reset_db_cache,
            filters=filters,
            order_by=order_by,
            distinct=distinct,
            columns=columns,
            max_rows=max_rows,
            cache_info=cache_info,
            show_meta_data=show_meta_data,
            dry_run=args.dry_run,
            renames=renames,
            set_index=set_index
        )
        sql_server.make_report()

        # sla de data frma op
        databases[data_base_key]["data"] = sql_server.data

    # report end time and duration
    end_time = pd.Timestamp.now(tz="Europe/Amsterdam")
    duration = (end_time - start_time) / pd.Timedelta(1, "s")
    logger.info(f"Ending {script_name} at {end_time}")
    logger.info("Total running time: {:.1f} s ".format(duration))


def _run():
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
