from tabulate import tabulate
import pandas as pd
from cbs_utils.readers import SbiInfo
from cbs_utils.misc import create_logger
import logging

logger = create_logger(console_log_level=logging.DEBUG)

file_name = '../data/SBI 2008 versie 2018.xlsx'
sbi = SbiInfo(file_name=file_name)

print(tabulate(sbi.data.head(20), headers="keys", tablefmt="psql"))

sbi.create_sbi_group(group_name="64.19.1-64.19.4+64.92.1-64.92.4",
                     group_label="Banken en Kredietbanken", prepend_main_level=True)

# maak twee nieuwe groepen
# sbi.create_sbi_group(group_name="64.19-64.92", group_label="Banken", prepend_main_level=True)
sbi.create_sbi_group(group_name="66.12-66.19", group_label="Financiële advisering")

# trek de nieuwe groepen uit de data frame
banken = sbi.data[sbi.data["group_label"] == "Banken en Kredietbanken"]
verzekeringen = sbi.data[sbi.data["group_label"] == "Financiële advisering"]

# combineer ze tot een nieuwe dataframe
sbi_new_group = pd.concat([banken, verzekeringen], axis=0)

# pas de volgorde van de kollommen aan
cn = sbi_new_group.columns.values
sbi_new_group = sbi_new_group[[cn[0], cn[2], cn[3], cn[1]]]

print(tabulate(sbi_new_group, headers="keys", tablefmt="psql"))

group_name = "10-12"
group_label = "Industrie"
sbi_column_key = "group_key"
sbi_column_label = "group_label"

sbi.create_sbi_group(group_name=group_name, group_label=group_label,
                     name_column_key=sbi_column_key,
                     label_column_key=sbi_column_label,
                     prepend_main_level=True,
                     rename_group={"-": "_"})

industrie = sbi.data[sbi.data[sbi_column_key] != ""]
cn = industrie.columns.values
sbi_new_group = industrie[[cn[0], cn[2], cn[3], cn[1]]]
print(tabulate(industrie, headers="keys", tablefmt="psql"))

group_name = "35-39"
group_label = "Afval"

afval = sbi.data[sbi.data[sbi_column_key] != ""]
cn = afval.columns.values
sbi_new_group = afval[[cn[0], cn[2], cn[3], cn[1]]]
print(tabulate(sbi_new_group, headers="keys", tablefmt="psql"))

# groups can be combined to one using a + or ,
group_name = "77-78,80-82"
group_label = "Rental"
sbi.create_sbi_group(group_name=group_name, group_label=group_label,
                     name_column_key=sbi_column_key,
                     label_column_key=sbi_column_label,
                     prepend_main_level=True,
                     rename_group={"-": "_"})

rental = sbi.data[sbi.data[sbi_column_key] != ""]
cn = rental.columns.values
sbi_new_group = rental[[cn[0], cn[2], cn[3], cn[1]]]
print(tabulate(sbi_new_group, headers="keys", tablefmt="psql"))

# als je alle supgroepen van group_name = "95.1" wilt moet je ze expliciet toevoegen. Alleen als
# je een eind code geeft worden ze allemaal toegevoegd
group_name = "95.11-95.12"
group_label = "Reparatie van computers en communicatieapparatuur"
sbi.create_sbi_group(group_name=group_name, group_label=group_label,
                     name_column_key=sbi_column_key,
                     label_column_key=sbi_column_label,
                     prepend_main_level=True,
                     rename_group={"-": "_"})

reparatie = sbi.data[sbi.data[sbi_column_key] != ""]
cn = reparatie.columns.values
sbi_new_group = reparatie[[cn[0], cn[2], cn[3], cn[1]]]
print(tabulate(sbi_new_group, headers="keys", tablefmt="psql"))
