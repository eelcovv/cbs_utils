# een paar korte voorbeelden om een ms sql data base te lezen
import logging
from cbs_utils.sql_server_utils import SqlServerReader

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("cbs_utils")

# voorbeeld van btw file
btw = SqlServerReader(
    server="S0DSQL0118P\\I01",
    database="BaselineVrs_Prd",
    table_info="BaselineVerstrekker.SQS211600_PRODUCTIE_ICT_GEBRUIKBEDRIJVEN_TMIN1_OUTPUT",
    max_rows=100
)
btw.make_report()

# de instellingen
server = "s0dsql0088p\\i01"
database = "EhoBouwstenenMirror_PRD"
tabel_naam = "[dbo].[CP_Koppelingen_naar_AE_en_SE]"
max_rows = 100  # lees alleen de eerste 100 records

column_selectie = [
    "StandId",
    "CbsPersoonIdentificatie",
    "BeIdentificatie",
    "KvkNummer",
    "CpJuridischeNaam",
    "CpHandelsNaam",
    "MaWerkzamePersonen",
    "MaSbiActueel",
]

# lees de eerste 100 regels van de de tabel
sql_server = SqlServerReader(
    server=server,
    database=database,
    table_info=tabel_naam,
    columns=column_selectie,
    max_rows=max_rows
)
sql_server.make_report()

# of filter de data op een aantal kolommen. In dit voorbeeld halen we alle rijgen op met standId gelijk aan 1901
# en slaan de legen kvk en beid velden over (not null)
sql_server2 = SqlServerReader(
    server=server,
    database=database,
    table_info=tabel_naam,
    columns=column_selectie,
    filters=dict(
        StandID="1901",
        KvkNummer="not null",
        BeIdentificatie="not null"
    )
)
sql_server2.make_report()

# merk op dat de data in een data frame opgeslagen is. Dus we kunnen doen
sql_server2.data.info()
sql_server2.data.describe()

